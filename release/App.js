"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");
// Creates and configures an ExpressJS web server.
class App {
    //Run configuration methods on the Express instance.
    constructor() {
        this.express = express();
        this.middleware();
        this.routes();
    }
    // Configure Express middleware.
    middleware() {
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
    }
    // Configure API endpoints.
    routes() {
        /* This is just to get up and running, and to make sure what we've got is
         * working so far. This function will change when we start to add more
         * API endpoints */
        let router = express.Router();
        // placeholder route handler
        router.get('/', (req, res, next) => {
            res.json([{
                    name: "Norte",
                    value: 0,
                    checked: 0
                }, {
                    name: "Sul",
                    value: 1,
                    checked: 1
                }, {
                    name: "Centro",
                    value: 2,
                    checked: 0
                }, {
                    name: "Internacional",
                    value: 3,
                    checked: 1
                }]);
        });
        this.express.use('/', router);
    }
}
exports.default = new App().express;
