"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AppBll {
    /**
     *
     */
    constructor() {
    }
    /**
     * GetDispGeog
     */
    GetDispGeog() {
        return [{
                name: "Norte city",
                value: 0,
                checked: 0
            }, {
                name: "Ananas city",
                value: 1,
                checked: 1
            }, {
                name: "batatas - fritas ",
                value: 2,
                checked: 0
            }, {
                name: "Tau",
                value: 3,
                checked: 1
            }];
    }
}
exports.default = new AppBll();
