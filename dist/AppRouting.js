"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");
const Location_1 = require("./Location");
const Common_1 = require("./Common");
const Project_1 = require("./Project");
// Creates and configures an ExpressJS web server.
class AppRouting {
    //Run configuration methods on the Express instance.
    constructor() {
        this.express = express();
        let cors = require('cors');
        this.express.use(cors());
        this.middleware();
        this.routes();
        this.Location = new Location_1.default();
        this.Common = new Common_1.default();
        this.Project = new Project_1.default();
    }
    // Configure Express middleware.
    middleware() {
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
    }
    // Configure API endpoints.
    routes() {
        /* This is just to get up and running, and to make sure what we've got is
        * working so far. This function will change when we start to add more
        * API endpoints */
        let router = express.Router();
        // placeholder route handler
        router.get('/Location/', (req, res, next) => {
            res.json(this.Location.Get());
        });
        router.get('/Common/GetExperienceLevels/', (req, res, next) => {
            res.json(this.Common.GetExperienceLevels());
        });
        router.get('/Skills/', (req, res, next) => {
            res.json(this.Common.getSkills());
        });
        router.get('/Projects/', (req, res, next) => {
            res.json(this.Project.Get());
        });
        this.express.use('/', router);
    }
}
exports.default = new AppRouting().express;
