"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Location {
    /**
     *
     */
    constructor() {
    }
    /**
     * GetDispGeog
     */
    Get() {
        return [{
                name: "Norte",
                value: 0,
                checked: 0
            }, {
                name: "Sul",
                value: 1,
                checked: 1
            }, {
                name: "Centro",
                value: 2,
                checked: 0
            }, {
                name: "International ",
                value: 3,
                checked: 1
            }];
    }
}
exports.default = Location;
