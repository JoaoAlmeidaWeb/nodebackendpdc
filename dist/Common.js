"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Common {
    /**
    *
    */
    constructor() {
    }
    /**
    * GetDispGeog
    */
    GetExperienceLevels() {
        return [{
                name: "Iniciado",
                value: 0
            }, {
                name: "Intermedio",
                value: 1
            }, {
                name: "Avançado",
                value: 2
            }, {
                name: "Expert",
                value: 3
            }];
    }
    getSkills() {
        return [{
                name: "Javascript",
                year: 2,
                value: 0,
                expId: 1
            }, {
                name: "HTML",
                year: 3,
                value: 1,
                expId: 2
            }, {
                name: "CSS",
                year: 2,
                value: 2,
                expId: 3
            }];
    }
}
exports.default = Common;
