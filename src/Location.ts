class Location {
    /**
     *
     */
    public constructor() {
        
    }

    /**
     * GetDispGeog
     */
    public Get() {
        return [{
            name: "Norte",
            value: 0,
            checked: 0
          }, {
            name: "Sul",
            value: 1,
            checked: 1
          }, {
            name: "Centro",
            value: 2,
            checked: 0
          }, {
            name: "International ",
            value: 3,
            checked: 1
          }];
    }
}

export default Location;