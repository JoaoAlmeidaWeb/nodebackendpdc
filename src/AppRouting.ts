import * as path from 'path';
import * as express from 'express';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';

import Location from './Location';
import Common from './Common';
import Project from './Project';

// Creates and configures an ExpressJS web server.
class AppRouting {

    // ref to Express instance
    public express: express.Application;
    private Location: Location;
    private Common: Common;
    private Project:Project;
    //Run configuration methods on the Express instance.
    constructor() {
        this.express = express();
        let cors = require('cors')
        this.express.use(cors());
        this.middleware();
        this.routes();
        this.Location = new Location();
        this.Common = new Common();
        this.Project = new Project();
    }

    // Configure Express middleware.
    private middleware(): void {
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
    }

    // Configure API endpoints.
    private routes(): void {
        /* This is just to get up and running, and to make sure what we've got is
        * working so far. This function will change when we start to add more
        * API endpoints */
        let router = express.Router();
        // placeholder route handler
        router.get('/Location/', (req, res, next) => {
            res.json(
                this.Location.Get()
            );
        });

  

            router.get('/Common/GetExperienceLevels/', (req,res,next) => {
                res.json(
                    this.Common.GetExperienceLevels()
                );
            });

            router.get('/Skills/', (req,res,next) => {
                res.json(
                    this.Common.getSkills()
                );
            }); 

            router.get('/Projects/', (req,res,next) => {
                res.json(
                    this.Project.Get()
                );
            });
            this.express.use('/', router);
        }

    }

    export default new AppRouting().express;