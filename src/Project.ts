class Project {
    /**
     *
     */
    public constructor() {
        
    }

    /**
     * GetDispGeog
     */
    public Get() {
        return(
        [{
          name: "projecto1",
          role: "web dev",
          tecnologies: ["javascript", "html"],
          beginningDate: "12/07/2017",
          endDate: "12/09/2018",
          description: "web project"
        },
        {
          name: "projecto2",
          role: "web dev",
          tecnologies: ["javascript", "css"],
          beginningDate: "12/07/2017",
          endDate: "12/09/2018",
          description: "web project"
        }
      ])
    }
}

export default Project;